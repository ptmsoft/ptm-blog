drop user if exists dataaccess;
drop database  if exists dataaccess;

create user dataaccess with password 'dataaccess';
create database dataaccess owner dataaccess;

\connect dataaccess;

create extension "uuid-ossp";

create table person (
    uuid    UUID not null primary key,
    first_name TEXT,
    last_name TEXT
);

alter table person owner to dataaccess;

insert into person values(uuid_generate_v4(), 'John', 'Smith');