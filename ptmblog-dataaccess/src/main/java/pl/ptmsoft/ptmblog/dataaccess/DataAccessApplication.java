package pl.ptmsoft.ptmblog.dataaccess;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.ptmsoft.ptmblog.dataaccess.domain.Person;
import pl.ptmsoft.ptmblog.dataaccess.repository.SprintDataJpaPersonRepository;
import pl.ptmsoft.ptmblog.dataaccess.repository.PersonRepository;

@SpringBootApplication
@Slf4j
public class DataAccessApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataAccessApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(SprintDataJpaPersonRepository ormPersonRepository, PersonRepository jdbcPersonRepository) {
        return args -> {
            ormPersonRepository.save(new Person("Piotr", "Nowak"));
            ormPersonRepository.save(new Person("Michał", "Jagiełło"));

            log.info("Osoby znalezione za pomocą rezpotyroium Spring Data JPA findAll():");
            log.info("-------------------------------");
            for (Person customer : ormPersonRepository.findAll()) {
                log.info(customer.toString());
            }
            log.info("");

            log.info("Osoby znalezione za pomocą rezpotyroium JDBC findAll():");
            log.info("-------------------------------");
            for (Person customer : jdbcPersonRepository.findAll()) {
                log.info(customer.toString());
            }
            log.info("");
        };
    }
}
