package pl.ptmsoft.ptmblog.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ptmsoft.ptmblog.dataaccess.domain.Person;

@Repository
public interface SprintDataJpaPersonRepository extends JpaRepository<Person, Long> {

}
