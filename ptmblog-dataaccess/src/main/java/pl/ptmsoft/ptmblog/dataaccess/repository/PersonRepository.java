package pl.ptmsoft.ptmblog.dataaccess.repository;

import pl.ptmsoft.ptmblog.dataaccess.domain.Person;

import java.util.List;

public interface PersonRepository {

    List<Person> findAll();
}
