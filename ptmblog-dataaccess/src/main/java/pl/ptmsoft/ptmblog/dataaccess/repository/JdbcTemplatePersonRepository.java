package pl.ptmsoft.ptmblog.dataaccess.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import pl.ptmsoft.ptmblog.dataaccess.domain.Person;

import java.util.List;

@Repository
public class JdbcTemplatePersonRepository implements PersonRepository {

    private static final String PG_FIND_ALL_PERSONS = "SELECT id, first_name, last_name FROM person";

    private final JdbcTemplate jdbcTemplate;
    private final RowMapper<Person> personRowMapper = (rs, i) -> new Person(
            rs.getLong("id"),
            rs.getString("first_name"),
            rs.getString("last_name")
    );

    public JdbcTemplatePersonRepository(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Person> findAll() {
        return jdbcTemplate.query(PG_FIND_ALL_PERSONS, personRowMapper);
    }
}
