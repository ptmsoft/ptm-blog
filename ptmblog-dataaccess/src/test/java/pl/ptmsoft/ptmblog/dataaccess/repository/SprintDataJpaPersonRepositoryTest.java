package pl.ptmsoft.ptmblog.dataaccess.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import pl.ptmsoft.ptmblog.dataaccess.domain.Person;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.context.annotation.FilterType.ASSIGNABLE_TYPE;

@DataJpaTest(includeFilters = {@Filter(type = ASSIGNABLE_TYPE, classes = PersonRepository.class)})
public class SprintDataJpaPersonRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SprintDataJpaPersonRepository repository;

    @Test
    public void testFindById() {
        Person person = new Person("first", "last");
        entityManager.persist(person);

        Optional<Person> result = repository.findById(person.getId());
        assertThat(result).isPresent().get().isEqualTo(person);
    }
}
